package com.example.rafhack.leaguetopchampions.data.entities

import java.util.*


class ChampionMastery {

    var chestGranted: Boolean = false
    var championLevel: Int = 0
    var championPoints: Int = 0
    var championId: Long = 0
    var playerId: Long = 0
    var championPointsUntilNextLevel: Long = 0
    var championPointsSinceLastLevel: Long = 0
    var lastPlayTime: Calendar? = null

}