package com.example.rafhack.leaguetopchampions.data.domain

import com.example.rafhack.leaguetopchampions.data.entities.ChampionMastery
import com.example.rafhack.leaguetopchampions.data.remote.ServiceGenerator
import com.example.rafhack.leaguetopchampions.data.remote.services.ChampionMasteryService
import io.reactivex.Observable
import io.reactivex.Single

class ChampionMasteryInteractor {

    private val service: ChampionMasteryService
        get() = ServiceGenerator.createService(ChampionMasteryService::class.java)


    fun getChampionMasteries(summonerId: Long): Single<List<ChampionMastery>> = service.getChampionMasteries(summonerId)

}