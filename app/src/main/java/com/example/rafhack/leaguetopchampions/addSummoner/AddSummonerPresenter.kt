package com.example.rafhack.leaguetopchampions.addSummoner

import com.example.rafhack.leaguetopchampions.LeagueTopChampionsApplication
import com.example.rafhack.leaguetopchampions.data.domain.SummonerInteractor
import com.example.rafhack.leaguetopchampions.data.entities.Region
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import io.reactivex.android.schedulers.AndroidSchedulers

class AddSummonerPresenter(val view: AddSummonerContract.View) : AddSummonerContract.UserActionsListener {

    private val summonerInteractor = SummonerInteractor()

    override fun searchSummoner(summonerName: String) {
        view.setProgress(true)
        summonerInteractor.getSummonerByName(summonerName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.setProgress(false)
                    view.showSearchResult(it)
                }, {
                    view.setProgress(false)
                    view.showSearchResult(null)
                })
    }

    override fun addSummoner(summoner: Summoner) {
        summonerInteractor.addSummoner(summoner)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.close()
                }, {
                    view.showSummonerAddError(it.message as String)
                })
    }

    override fun saveRegion(region: String) {
        LeagueTopChampionsApplication.region = Region.from(region)
    }
}