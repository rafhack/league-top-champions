package com.example.rafhack.leaguetopchampions.data.entities.staticData

data class StaticData<T>(var data: Map<Long, T?>? = null)