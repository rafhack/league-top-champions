package com.example.rafhack.leaguetopchampions.summonerDetail

import com.example.rafhack.leaguetopchampions.data.entities.ChampionMastery
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import com.example.rafhack.leaguetopchampions.data.entities.staticData.StaticData

interface SummonerDetailContract {

    interface View {
        fun setProgress(active: Boolean)
        fun showTopChampions(champions: StaticData<Champion>, masteries: List<ChampionMastery>)
        fun showChampionDetail(champion: Champion)
        fun showLoadingTopChampionsError()
    }

    interface UserActionsListener {
        fun loadTopChampions(summoner: Summoner)
        fun openChampionDetail(champion: Champion)
    }

}