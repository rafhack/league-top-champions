package com.example.rafhack.leaguetopchampions.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import com.example.rafhack.leaguetopchampions.R
import kotterknife.bindView

@SuppressLint("Registered")
open class BaseProgressActivity : AppCompatActivity() {

    //region Fields
    private val frmRoot by bindView<ConstraintLayout>(R.id.activity_base_progress_frm_root)
    private val frmProgress by bindView<FrameLayout>(R.id.activity_base_progress_frm_progress)
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_base_progress)
    }

    override fun setContentView(layoutResID: Int) {
        val frmContent: FrameLayout = frmRoot.findViewById(R.id.activity_base_progress_frm_content)
        layoutInflater.inflate(layoutResID, frmContent, true)
        super.setContentView(frmRoot)
    }
    //endregion

    //region Protected functions
    protected fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val ab = supportActionBar
        ab!!.setTitle(R.string.app_name)
    }

    protected fun showProgress() {
        frmProgress.visibility = VISIBLE
    }

    protected fun hideProgress() {
        frmProgress.visibility = GONE
    }

    protected fun showSnack(message: String) {
        Snackbar.make(findViewById(R.id.activity_base_progress_frm_root),
                message, Snackbar.LENGTH_SHORT).show()
    }
    //endregion

}