package com.example.rafhack.leaguetopchampions.addSummoner

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.example.rafhack.leaguetopchampions.R
import com.example.rafhack.leaguetopchampions.data.entities.Region
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import com.example.rafhack.leaguetopchampions.utils.loadWithGlide


class AddSummonerDialogFragment : AppCompatDialogFragment(), AddSummonerContract.View {

    //region Fields
    var updateListener: UpdateListener? = null
    val actionListener: AddSummonerContract.UserActionsListener = AddSummonerPresenter(this)
    private var summoner: Summoner? = null

    private lateinit var spnRegions: Spinner
    private lateinit var edtSummonerName: EditText
    private lateinit var btnSearch: Button
    private lateinit var btnOk: Button
    private lateinit var btnCancel: Button
    private lateinit var pgbLoading: ProgressBar
    private lateinit var ctlSummoner: ConstraintLayout
    private lateinit var tvwSummonerName: TextView
    private lateinit var imgSummonerIcon: ImageView
    //endregion

    //region Lifecycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)

        val view = inflater.inflate(R.layout.dialog_fragment_add_summoner, container, false)

        spnRegions = view.findViewById(R.id.dialog_fragment_add_summoner_spn_regions)
        edtSummonerName = view.findViewById(R.id.dialog_fragment_add_summoner_edt_summoner_name)
        btnSearch = view.findViewById(R.id.dialog_fragment_add_summoner_btn_search)
        btnOk = view.findViewById(R.id.dialog_fragment_add_summoner_btn_add)
        btnCancel = view.findViewById(R.id.dialog_fragment_add_summoner_btn_cancel)
        pgbLoading = view.findViewById(R.id.dialog_fragment_add_summoner_pgb)
        ctlSummoner = view.findViewById(R.id.dialog_fragment_add_summoner_ctl_summoner)
        tvwSummonerName = view.findViewById(R.id.dialog_fragment_add_summoner_tvw_summoner_name)
        imgSummonerIcon = view.findViewById(R.id.dialog_fragment_add_summoner_img_summoner)

        setupView()

        return view
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        return dialog
    }

    override fun onResume() {
        super.onResume()

        val params = dialog.window!!.attributes
        params.width = resources.getDimensionPixelSize(R.dimen.dialog_fragment_width)
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as android.view.WindowManager.LayoutParams
    }
    //endregion

    //region Private functions
    private fun setupView() {

        btnSearch.setOnClickListener {
            actionListener.searchSummoner(edtSummonerName.text.toString())
        }

        btnCancel.setOnClickListener { dismiss() }

        btnOk.setOnClickListener {
            actionListener.addSummoner(summoner!!)
        }

        setupSpinner()
    }

    private fun setupSpinner() {
        val adapter = RegionAdapter(Region.values())
        spnRegions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) = Unit

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                actionListener.saveRegion(adapter.regions?.get(position)?.endPoint as String)
            }
        }
        spnRegions.adapter = adapter
    }
    //endregion

    //region Overridden View Functions
    override fun setProgress(active: Boolean) {
        if (active) {
            ctlSummoner.visibility = VISIBLE
            pgbLoading.visibility = VISIBLE
        } else {
            pgbLoading.visibility = GONE
        }
    }

    override fun showSearchResult(summoner: Summoner?) {
        if (summoner == null) {
            tvwSummonerName.setText(R.string.rift_cannot_find_summoner)
            imgSummonerIcon.visibility = GONE
            btnOk.isEnabled = false
        } else {
            this.summoner = summoner
            tvwSummonerName.text = summoner.name
            imgSummonerIcon.visibility = VISIBLE
            imgSummonerIcon.loadWithGlide("profileicon/" + summoner.profileIconId + ".png")
            btnOk.isEnabled = true
        }
    }

    override fun close() {
        updateListener?.onAddSummoner(summoner!!)
        dismiss()
    }

    override fun showSummonerAddError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
    //endregion

    //region Inner classes
    inner class RegionAdapter(regions: Array<Region>) : ArrayAdapter<String>(
            context, R.layout.simple_spinner_item) {

        var regions: Array<Region>? = null

        init {
            this.regions = regions
            setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        }

        override fun getCount(): Int = regions?.size as Int

        override fun getItem(position: Int): String = getString(regions?.get(position)?.strRes as Int)
    }
    //endregion
}