package com.example.rafhack.leaguetopchampions.championDetail

import com.example.rafhack.leaguetopchampions.data.domain.StaticDataInteractor
import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import io.reactivex.android.schedulers.AndroidSchedulers

class ChampionDetailPresenter(private val view: ChampionDetailContract.View) : ChampionDetailContract.UserActionsListener {

    private val staticDataInteractor = StaticDataInteractor()

    override fun loadChampionDetails(champion: Champion) {
        view.setProgress(true)
        staticDataInteractor.getChampionById(champion.id.toLong())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.setProgress(false)
                    view.showChampionDetails(it)
                }, {
                    view.setProgress(false)
                    view.showChampionLoadingError()
                })
    }

}