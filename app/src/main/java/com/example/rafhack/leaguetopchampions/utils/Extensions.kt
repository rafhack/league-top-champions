package com.example.rafhack.leaguetopchampions.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.rafhack.leaguetopchampions.BuildConfig
import com.example.rafhack.leaguetopchampions.LeagueTopChampionsApplication

fun ImageView.loadWithGlide(url: String?) {
    Glide.with(this.context)
            .load(url?.versionedImageUrl())
            .crossFade()
            .into(this)
}

fun String.versionedImageUrl(): String {
    return BuildConfig.STATIC_VERSIONED_URL.plus(this).format(LeagueTopChampionsApplication.version)
}