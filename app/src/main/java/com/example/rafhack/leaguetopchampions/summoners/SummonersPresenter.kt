package com.example.rafhack.leaguetopchampions.summoners

import com.example.rafhack.leaguetopchampions.LeagueTopChampionsApplication
import com.example.rafhack.leaguetopchampions.data.domain.StaticDataInteractor
import com.example.rafhack.leaguetopchampions.data.domain.SummonerInteractor
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import io.reactivex.android.schedulers.AndroidSchedulers

class SummonersPresenter(val view: SummonersContract.View) : SummonersContract.UserActionsListener {

    private val summonerInteractor = SummonerInteractor()
    private val staticDataInteractor = StaticDataInteractor()

    override fun loadSummoners() {
        view.setProgress(true)
        if (LeagueTopChampionsApplication.version.isEmpty())
            getVersion()
        else
            summonerInteractor.getAllSummoners()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        view.setProgress(false)
                        view.showSummoners(it)
                    }, {
                        view::showSummonerLoadingErrorMessage
                        view.setProgress(false)
                    })
    }

    override fun addNewSummoner() {
        view.showAddSummoner()
    }

    override fun openSummonerDetail(summoner: Summoner) {
        view.showSummonerDetail(summoner)
    }

    override fun getVersion() {
        staticDataInteractor.getVersions()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    LeagueTopChampionsApplication.version = it.first()
                    loadSummoners()
                }, {
                    view.showSummonerLoadingErrorMessage()
                    view.setProgress(false)
                })
    }
}