package com.example.rafhack.leaguetopchampions.summonerDetail

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.rafhack.leaguetopchampions.R
import com.example.rafhack.leaguetopchampions.data.entities.ChampionMastery
import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import com.example.rafhack.leaguetopchampions.data.entities.staticData.StaticData
import com.example.rafhack.leaguetopchampions.utils.loadWithGlide

class SummonerDetailAdapter(private val masteries: List<ChampionMastery>?,
                            private val champions: StaticData<Champion>?,
                            private val listener: ItemClickListener) : RecyclerView.Adapter<SummonerDetailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummonerDetailAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.champion_item,
                parent, false))
    }

    override fun getItemCount(): Int = masteries?.size!!

    override fun onBindViewHolder(holder: SummonerDetailAdapter.ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var tvwChampRank: TextView = itemView.findViewById(R.id.champion_rank_item_tvw_rank)
        private var tvwChampName: TextView = itemView.findViewById(R.id.champion_rank_item_tvw_champ_name)
        private var tvwChampDesc: TextView = itemView.findViewById(R.id.champion_rank_item_tvw_champ_desc)
        private var tvwChampPnts: TextView = itemView.findViewById(R.id.champion_rank_item_tvw_champ_points)
        private var imgChampFace: ImageView = itemView.findViewById(R.id.champion_rank_item_img_champ)

        fun bind(position: Int) {
            val mastery: ChampionMastery? = masteries?.get(position)
            val champion: Champion? = champions?.data?.get(mastery?.championId)

            tvwChampName.text = champion?.name
            tvwChampRank.text = (1 + position).toString()
            tvwChampDesc.text = champion?.title
            tvwChampPnts.text = mastery?.championPoints.toString()
            imgChampFace.loadWithGlide("champion/".plus(champion?.key).plus(".png"))
            itemView.setOnClickListener { listener.onChampionClick(champion!!) }
        }
    }

    interface ItemClickListener {
        fun onChampionClick(champion: Champion)
    }
}