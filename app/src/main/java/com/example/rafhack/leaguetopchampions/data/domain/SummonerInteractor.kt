package com.example.rafhack.leaguetopchampions.data.domain

import android.arch.persistence.room.Room
import com.example.rafhack.leaguetopchampions.LeagueTopChampionsApplication
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import com.example.rafhack.leaguetopchampions.data.local.AppDatabase
import com.example.rafhack.leaguetopchampions.data.remote.ServiceGenerator
import com.example.rafhack.leaguetopchampions.data.remote.services.SummonerService
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class SummonerInteractor {

    var db
        get() = Room.databaseBuilder(LeagueTopChampionsApplication.application?.applicationContext!!,
                AppDatabase::class.java, "database").build()
        set(value) {}

    private val service
        get() = ServiceGenerator.createService(SummonerService::class.java)

    fun getAllSummoners(): Flowable<MutableList<Summoner>> {
        return db.summonerDAO().getAll()
    }

    fun addSummoner(summoner: Summoner): Single<AppDatabase> {
        return Single.just(db)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { it.summonerDAO().insertAll(summoner) }
    }

    fun getSummonerByName(summonerName: String): Single<Summoner> {
        return service.getSummonerByName(summonerName)
    }


}