package com.example.rafhack.leaguetopchampions.summoners

import com.example.rafhack.leaguetopchampions.data.entities.Summoner

interface SummonersContract {

    interface View {
        fun setProgress(active: Boolean)
        fun showSummoners(summoners: MutableList<Summoner>)
        fun showAddSummoner()
        fun showSummonerDetail(summoner: Summoner)
        fun showSummonerLoadingErrorMessage()
    }

    interface UserActionsListener {
        fun loadSummoners()
        fun addNewSummoner()
        fun openSummonerDetail(summoner: Summoner)
        fun getVersion()
    }

}