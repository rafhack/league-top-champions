package com.example.rafhack.leaguetopchampions.championDetail

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.example.rafhack.leaguetopchampions.R
import com.example.rafhack.leaguetopchampions.base.BaseProgressActivity
import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import com.example.rafhack.leaguetopchampions.utils.loadWithGlide
import kotterknife.bindView
import org.parceler.Parcels

class ChampionDetailActivity : BaseProgressActivity(), ChampionDetailContract.View {

    //region Fields
    private lateinit var champion: Champion
    private val actionsListener = ChampionDetailPresenter(this)

    private val imgChampFace by bindView<ImageView>(R.id.activity_champion_detail_img_champion)
    private val tvwChampionName by bindView<TextView>(R.id.activity_champion_detail_tvw_name)
    private val tvwChampionDesc by bindView<TextView>(R.id.activity_champion_detail_tvw_desc)
    private val tvwChampionLore by bindView<TextView>(R.id.activity_champion_detail_tvw_lore)
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_champion_detail)
        champion = Parcels.unwrap(intent.getParcelableExtra("champion"))
        actionsListener.loadChampionDetails(champion)
        setupToolbar()
        supportActionBar?.title = champion.name
    }
    //endregion

    //region Private functions
    private fun setupView() {
        imgChampFace.loadWithGlide("champion/".plus(champion.key).plus(".png"))
        tvwChampionName.text = champion.name
        tvwChampionDesc.text = champion.title
        tvwChampionLore.text = champion.lore
    }
    //endregion

    //region Overridden View functions
    override fun setProgress(active: Boolean) {
        if (active) showProgress() else hideProgress()
    }

    override fun showChampionDetails(champion: Champion) {
        this.champion = champion
        setupView()
    }

    override fun showChampionLoadingError() {
        showSnack(getString(R.string.rift_cannot_get_champions))
    }
    //endregion

}