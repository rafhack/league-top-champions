package com.example.rafhack.leaguetopchampions

import android.app.Application
import com.example.rafhack.leaguetopchampions.data.entities.Region
import com.example.rafhack.leaguetopchampions.data.entities.Summoner

class LeagueTopChampionsApplication : Application() {

    companion object {
        var application: LeagueTopChampionsApplication? = null
        var region: Region = Region.BR
        var summoner: Summoner? = null
        var version: String = ""
    }

    override fun onCreate() {
        super.onCreate()
        application = this
    }

}