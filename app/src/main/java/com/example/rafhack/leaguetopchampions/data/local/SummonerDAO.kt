package com.example.rafhack.leaguetopchampions.data.local

import android.arch.persistence.room.*
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import io.reactivex.Flowable

@Dao
interface SummonerDAO {

    @Query("SELECT * FROM summoner")
    fun getAll(): Flowable<MutableList<Summoner>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg users: Summoner)

    @Delete
    fun delete(summoner: Summoner)

}