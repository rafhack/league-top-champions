package com.example.rafhack.leaguetopchampions.data.domain

import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import com.example.rafhack.leaguetopchampions.data.entities.staticData.StaticData
import com.example.rafhack.leaguetopchampions.data.remote.ServiceGenerator
import com.example.rafhack.leaguetopchampions.data.remote.services.StaticDataService
import io.reactivex.Observable
import io.reactivex.Single


class StaticDataInteractor {

    private val serviceCached: StaticDataService
        get() = ServiceGenerator.createService(StaticDataService::class.java, useForcedCache = true)

    fun getChampionById(championId: Long): Single<Champion> = serviceCached.getChampionById(championId)

    fun getAllChampions(): Single<StaticData<Champion>> = serviceCached.getAllChampions()

    fun getVersions(): Single<List<String>> = serviceCached.getVersions()
}