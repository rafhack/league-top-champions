package com.example.rafhack.leaguetopchampions.data.entities.staticData

import org.parceler.Parcel

@Parcel
data class Champion(
        var title: String = "",
        var id: Int = 0,
        var key: String = "",
        var name: String = "",
        var lore: String = ""
)