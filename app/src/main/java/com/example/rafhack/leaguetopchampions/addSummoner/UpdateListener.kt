package com.example.rafhack.leaguetopchampions.addSummoner

import com.example.rafhack.leaguetopchampions.data.entities.Summoner

interface UpdateListener {
    fun onAddSummoner(summoner: Summoner)
}