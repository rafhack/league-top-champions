package com.example.rafhack.leaguetopchampions.summoners

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.rafhack.leaguetopchampions.R
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import com.example.rafhack.leaguetopchampions.utils.loadWithGlide

class SummonersAdapter(private val summoners: MutableList<Summoner>, private val listener: ItemClickListener) :
        RecyclerView.Adapter<SummonersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.summoner_item,
                parent, false))
    }

    override fun getItemCount(): Int = summoners.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val imgSummoner = itemView.findViewById<ImageView>(R.id.summoner_item_img_summoner)
        private val tvwSummonerName = itemView.findViewById<TextView>(R.id.summoner_item_tvw_summoner_name)

        fun bind(position: Int) {
            val summoner = summoners[position]
            imgSummoner.loadWithGlide("profileicon/" + summoner.profileIconId + ".png")
            tvwSummonerName.text = summoner.name
            itemView.setOnClickListener { listener.onSummonerClick(summoner) }
        }
    }

    interface ItemClickListener {
        fun onSummonerClick(summoner: Summoner)
    }

}