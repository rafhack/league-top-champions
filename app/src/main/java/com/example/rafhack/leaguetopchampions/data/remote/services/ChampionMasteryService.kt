package com.example.rafhack.leaguetopchampions.data.remote.services

import com.example.rafhack.leaguetopchampions.data.entities.ChampionMastery
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ChampionMasteryService {

    @GET("champion-mastery/v3/champion-masteries/by-summoner/{summonerId}")
    fun getChampionMasteries(@Path("summonerId") summonerId: Long): Single<List<ChampionMastery>>

}