package com.example.rafhack.leaguetopchampions.championDetail

import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion

interface ChampionDetailContract {

    interface View {
        fun setProgress(active: Boolean)
        fun showChampionDetails(champion: Champion)
        fun showChampionLoadingError()
    }

    interface UserActionsListener {
        fun loadChampionDetails(champion: Champion)
    }

}