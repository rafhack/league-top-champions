package com.example.rafhack.leaguetopchampions.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.rafhack.leaguetopchampions.data.entities.Summoner

@Database(entities = [(Summoner::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun summonerDAO(): SummonerDAO
}