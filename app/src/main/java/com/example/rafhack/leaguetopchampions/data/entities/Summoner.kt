package com.example.rafhack.leaguetopchampions.data.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

@Parcel(Parcel.Serialization.BEAN)
@Entity
data class Summoner(
        @PrimaryKey
        @SerializedName(value = "id", alternate = ["summonerId"])
        var id: Long = 0,

        @ColumnInfo(name = "profile_icon_id")
        var profileIconId: Int = 0,

        @ColumnInfo(name = "name")
        @SerializedName(value = "name", alternate = ["summonerName"])
        var name: String = "",

        @ColumnInfo(name = "summoner_level")
        var summonerLevel: Long = 0,

        @ColumnInfo(name = "account_id")
        var accountId: Long = 0
)
