package com.example.rafhack.leaguetopchampions.data.remote.services

import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


interface SummonerService {

    @GET("summoner/v3/summoners/by-name/{summonerName}")
    fun getSummonerByName(@Path("summonerName") summonerName: String): Single<Summoner>

}
