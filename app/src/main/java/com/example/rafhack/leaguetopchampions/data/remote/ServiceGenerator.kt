package com.example.rafhack.leaguetopchampions.data.remote

import android.text.TextUtils
import com.example.rafhack.leaguetopchampions.BuildConfig
import com.example.rafhack.leaguetopchampions.LeagueTopChampionsApplication
import com.google.gson.GsonBuilder
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

object ServiceGenerator {

    //region FIELDS
    private const val DEFAULT_TIMEOUT: Long = 60
    private const val CACHE_SIZE: Long = 10 * 1024 * 1024
    private const val MAX_STALE = 60 * 60 * 48
    private const val MAX_AGE = 60 * 60 * 24
    private var authorization: String? = null
    private var useForcedCache: Boolean = false
    //endregion

    //region METHODS

    //region PUBLIC METHODS
    fun <S> createService(serviceClass: Class<S>, useForcedCache: Boolean = false): S {
        authorization = BuildConfig.API_KEY
        ServiceGenerator.useForcedCache = useForcedCache

        val cacheDirectory = File(LeagueTopChampionsApplication.application?.externalCacheDir?.absolutePath, "OKHttpCache")

        val builder = OkHttpClient.Builder()
                .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(defaultInterceptor)
                .addInterceptor(loggingInterceptor)

        if (useForcedCache) {
            try {
                val cache = Cache(cacheDirectory, CACHE_SIZE)
                builder.cache(cache)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .registerTypeAdapter(Calendar::class.java, CalendarSerializer())
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL.format(LeagueTopChampionsApplication.region.endPoint))
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()

        return retrofit.create(serviceClass)
    }
    //endregion

    //region PRIVATE METHODS
    private val defaultInterceptor: Interceptor
        get() = Interceptor { chain ->

            val builder = chain.request().newBuilder()

            if (!TextUtils.isEmpty(authorization))
                builder.addHeader("X-Riot-Token", authorization!!)

            val request = builder.build()
            val resp: Response = chain.proceed(request)

            if (useForcedCache)
                resp.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=$MAX_STALE, max-age=$MAX_AGE")
                        .build()
            else resp
        }

    private val loggingInterceptor: Interceptor
        get() {
            val logging = HttpLoggingInterceptor()
            logging.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            return logging
        }
    //endregion

    //endregion
}