package com.example.rafhack.leaguetopchampions.data.remote.services

import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import com.example.rafhack.leaguetopchampions.data.entities.staticData.StaticData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface StaticDataService {

    @GET("static-data/v3/champions/{championId}")
    fun getChampionById(@Path("championId") championId: Long, @Query("champData") data: String = "lore"): Single<Champion>

    @GET("static-data/v3/champions")
    fun getAllChampions(@Query("dataById") byId: Boolean = true): Single<StaticData<Champion>>

    @GET("static-data/v3/versions")
    fun getVersions(): Single<List<String>>

}