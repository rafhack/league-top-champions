package com.example.rafhack.leaguetopchampions.addSummoner

import com.example.rafhack.leaguetopchampions.data.entities.Summoner

interface AddSummonerContract {

    interface View {
        fun setProgress(active: Boolean)
        fun showSearchResult(summoner: Summoner?)
        fun close()
        fun showSummonerAddError(message: String)
    }

    interface UserActionsListener {
        fun searchSummoner(summonerName: String)
        fun addSummoner(summoner: Summoner)
        fun saveRegion(region: String)
    }

}