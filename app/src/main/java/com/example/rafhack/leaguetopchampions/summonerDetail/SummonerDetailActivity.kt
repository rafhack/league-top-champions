package com.example.rafhack.leaguetopchampions.summonerDetail

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.rafhack.leaguetopchampions.R
import com.example.rafhack.leaguetopchampions.base.BaseProgressActivity
import com.example.rafhack.leaguetopchampions.championDetail.ChampionDetailActivity
import com.example.rafhack.leaguetopchampions.data.entities.ChampionMastery
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import com.example.rafhack.leaguetopchampions.data.entities.staticData.StaticData
import kotterknife.bindView
import org.parceler.Parcels

class SummonerDetailActivity : BaseProgressActivity(), SummonerDetailContract.View {

    //region Fields
    private val actionListener = SummonerDetailPresenter(this)
    private lateinit var summoner: Summoner

    private val rcvChampions by bindView<RecyclerView>(R.id.activity_summoner_detail_rcv)
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summoner_detail)
        summoner = Parcels.unwrap(intent.getParcelableExtra("summoner"))
        actionListener.loadTopChampions(summoner)
        setupView()

        setupToolbar()
        supportActionBar?.title = summoner.name
    }
    //endregion

    //region Private functions
    private fun setupView() {
        rcvChampions.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false)
    }
    //endregion

    //region Overridden View functions
    override fun setProgress(active: Boolean) {
        if (active) showProgress() else hideProgress()
    }

    override fun showTopChampions(champions: StaticData<Champion>, masteries: List<ChampionMastery>) {
        rcvChampions.adapter = SummonerDetailAdapter(masteries, champions, object : SummonerDetailAdapter.ItemClickListener {
            override fun onChampionClick(champion: Champion) {
                actionListener.openChampionDetail(champion)
            }
        })
    }

    override fun showChampionDetail(champion: Champion) {
        val intent = Intent(this, ChampionDetailActivity::class.java)
        intent.putExtra("champion", Parcels.wrap(champion))
        startActivity(intent)
    }

    override fun showLoadingTopChampionsError() {
        showSnack(getString(R.string.rift_cannot_get_champions))
    }
    //endregion
}