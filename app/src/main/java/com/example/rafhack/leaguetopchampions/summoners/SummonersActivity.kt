package com.example.rafhack.leaguetopchampions.summoners

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View.VISIBLE
import com.example.rafhack.leaguetopchampions.R
import com.example.rafhack.leaguetopchampions.addSummoner.AddSummonerDialogFragment
import com.example.rafhack.leaguetopchampions.addSummoner.UpdateListener
import com.example.rafhack.leaguetopchampions.base.BaseProgressActivity
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import com.example.rafhack.leaguetopchampions.summonerDetail.SummonerDetailActivity
import kotterknife.bindView
import org.parceler.Parcels

class SummonersActivity : BaseProgressActivity(), SummonersContract.View {

    //region Fields
    private val actionListener: SummonersContract.UserActionsListener = SummonersPresenter(this)

    private val fabAddSummoner by bindView<FloatingActionButton>(R.id.activity_base_progress_fab)
    private val rcvSummoners by bindView<RecyclerView>(R.id.activity_summoners_rcv)
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summoners)

        setupToolbar()
        setupView()
        actionListener.loadSummoners()
    }
    //endregion

    //region Private functions
    private fun setupView() {
        fabAddSummoner.visibility = VISIBLE
        fabAddSummoner.setOnClickListener { actionListener.addNewSummoner() }
        rcvSummoners.layoutManager = GridLayoutManager(this, 3,
                GridLayoutManager.VERTICAL, false)
    }
    //endregion

    //region Overridden View functions
    override fun setProgress(active: Boolean) {
        if (active) showProgress() else hideProgress()
    }

    override fun showSummoners(summoners: MutableList<Summoner>) {
        rcvSummoners.adapter = SummonersAdapter(summoners, object : SummonersAdapter.ItemClickListener {
            override fun onSummonerClick(summoner: Summoner) {
                actionListener.openSummonerDetail(summoner)
            }
        })
    }

    override fun showSummonerLoadingErrorMessage() {
        showSnack(getString(R.string.rift_cannot_find_summoner))
    }

    override fun showAddSummoner() {
        val addSUmmonerDF = AddSummonerDialogFragment()
        addSUmmonerDF.updateListener = object : UpdateListener {
            override fun onAddSummoner(summoner: Summoner) {
                actionListener.loadSummoners()
            }
        }
        addSUmmonerDF.show(supportFragmentManager, "")
    }

    override fun showSummonerDetail(summoner: Summoner) {
        val intent = Intent(this, SummonerDetailActivity::class.java)
        intent.putExtra("summoner", Parcels.wrap(summoner))
        startActivity(intent)
    }
    //endregion

}