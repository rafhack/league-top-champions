package com.example.rafhack.leaguetopchampions.summonerDetail

import com.example.rafhack.leaguetopchampions.data.domain.ChampionMasteryInteractor
import com.example.rafhack.leaguetopchampions.data.domain.StaticDataInteractor
import com.example.rafhack.leaguetopchampions.data.entities.ChampionMastery
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import com.example.rafhack.leaguetopchampions.data.entities.staticData.StaticData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction

class SummonerDetailPresenter(val view: SummonerDetailContract.View) :
        SummonerDetailContract.UserActionsListener {

    private val champMasteryInteractor: ChampionMasteryInteractor = ChampionMasteryInteractor()
    private val staticDataInteractor: StaticDataInteractor = StaticDataInteractor()

    override fun loadTopChampions(summoner: Summoner) {
        view.setProgress(true)
        Single.zip(
                champMasteryInteractor.getChampionMasteries(summoner.id),
                staticDataInteractor.getAllChampions(), BiFunction<List<ChampionMastery>, StaticData<Champion>,
                Pair<List<ChampionMastery>, StaticData<Champion>>> { t1, t2 -> return@BiFunction Pair(t1, t2) })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.setProgress(false)
                    view.showTopChampions(it.second, it.first)
                }, {
                    view.setProgress(false)
                    view.showLoadingTopChampionsError()
                })
    }

    override fun openChampionDetail(champion: Champion) {
        view.showChampionDetail(champion)
    }

}