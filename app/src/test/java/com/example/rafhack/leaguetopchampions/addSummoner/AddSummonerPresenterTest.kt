package com.example.rafhack.leaguetopchampions.addSummoner

import com.example.rafhack.leaguetopchampions.LeagueTopChampionsApplication
import com.example.rafhack.leaguetopchampions.data.domain.SummonerInteractor
import com.example.rafhack.leaguetopchampions.data.entities.Region
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner.Silent::class)
class AddSummonerPresenterTest {

    @Mock
    private val addSummonerView: AddSummonerContract.View? = null
    private lateinit var addSummonerPresenter: AddSummonerPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        LeagueTopChampionsApplication.region = Region.BR
        addSummonerPresenter = AddSummonerPresenter(addSummonerView!!)
    }

    @Test
    fun searchSummoner() {
        val summoner = Summoner()
        doReturn(Single.just(summoner)).`when`(mock(SummonerInteractor::class.java)).getSummonerByName("Summ")
        addSummonerPresenter.searchSummoner("Summ")
        verify(addSummonerView)?.showSearchResult(any())
    }

    @Test
    fun saveRegion() {
        addSummonerPresenter.saveRegion("br1")
        assertEquals(LeagueTopChampionsApplication.region, Region.BR)
    }

    @After
    fun validate() {
        validateMockitoUsage()
    }

}