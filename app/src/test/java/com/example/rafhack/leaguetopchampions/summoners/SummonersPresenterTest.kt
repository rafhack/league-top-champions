package com.example.rafhack.leaguetopchampions.summoners

import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SummonersPresenterTest {

    @Mock
    private val summonerView: SummonersContract.View? = null
    private lateinit var summonersPresenter: SummonersPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        summonersPresenter = SummonersPresenter(summonerView!!)
    }

    @Test
    fun clickOnFABAddSummoner() {
        summonersPresenter.addNewSummoner()
        verify(summonerView)?.showAddSummoner()
    }

    @Test
    fun clickOnSummoner() {
        val summoner = mock(Summoner::class.java)
        summonersPresenter.openSummonerDetail(summoner)
        verify(summonerView)?.showSummonerDetail(summoner)
    }

}