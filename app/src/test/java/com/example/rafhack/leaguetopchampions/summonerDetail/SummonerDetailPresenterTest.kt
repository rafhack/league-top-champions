package com.example.rafhack.leaguetopchampions.summonerDetail

import com.example.rafhack.leaguetopchampions.LeagueTopChampionsApplication
import com.example.rafhack.leaguetopchampions.data.entities.ChampionMastery
import com.example.rafhack.leaguetopchampions.data.entities.Region
import com.example.rafhack.leaguetopchampions.data.entities.Summoner
import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import com.example.rafhack.leaguetopchampions.data.entities.staticData.StaticData
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.functions.BiFunction
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SummonerDetailPresenterTest {

    @Mock
    private val summonerDetailView: SummonerDetailContract.View? = null
    private lateinit var summonerDetailPresenter: SummonerDetailPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        LeagueTopChampionsApplication.region = Region.BR
        summonerDetailPresenter = SummonerDetailPresenter(summonerDetailView!!)
    }

    @Test
    fun openChampionDetail() {
        val champion = Champion()
        summonerDetailPresenter.openChampionDetail(champion)
        verify(summonerDetailView)?.showChampionDetail(champion)
    }

}