package com.example.rafhack.leaguetopchampions.championDetails

import com.example.rafhack.leaguetopchampions.LeagueTopChampionsApplication
import com.example.rafhack.leaguetopchampions.championDetail.ChampionDetailContract
import com.example.rafhack.leaguetopchampions.championDetail.ChampionDetailPresenter
import com.example.rafhack.leaguetopchampions.data.domain.StaticDataInteractor
import com.example.rafhack.leaguetopchampions.data.entities.Region
import com.example.rafhack.leaguetopchampions.data.entities.staticData.Champion
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
class ChampionDetailPresenterTest {

    @Mock
    private val championDetailView: ChampionDetailContract.View? = null
    private lateinit var championDetailPresenter: ChampionDetailContract.UserActionsListener

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        LeagueTopChampionsApplication.region = Region.BR
        championDetailPresenter = ChampionDetailPresenter(championDetailView!!)
    }

    @Test
    fun loadChampionDetails() {
        Mockito.doReturn(Single.just(Champion())).`when`(mock(StaticDataInteractor::class.java))
                .getChampionById(202)
        championDetailPresenter.loadChampionDetails(Champion())
        verify(championDetailView)?.showChampionDetails(Champion())
    }

    @After
    fun validate() {
        Mockito.validateMockitoUsage()
    }

}